const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),

  });

  await schema.validateAsync(req.body);

  next();
};

module.exports.validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .required(),

    password: Joi.string()
        .required(),

  });

  await schema.validateAsync(req.body);

  next();
};

module.exports.validateChangePassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .required(),

    newPassword: Joi.string()
        .required(),

  });

  await schema.validateAsync(req.body);

  next();
};

module.exports.validateAddNote = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);

  next();
};

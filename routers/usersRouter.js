const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {getUserData, deleteUser, changePassword,
  submitPassword} = require('../controllers/usersController');
const {validateChangePassword} = require('./middlewares/validationMiddleware');

router.use('/me', authMiddleware);

router.get('/me', asyncWrapper(getUserData));

router.delete('/me', asyncWrapper(deleteUser));

router.patch('/me', validateChangePassword,
    asyncWrapper(submitPassword), asyncWrapper(changePassword));

module.exports = router;

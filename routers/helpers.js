/**
 * Execute function as async
 * @param {object} callback - some function
 * @return {function} - function result
 */
function asyncWrapper(callback) {
  return (req, res, next) => {
    callback(req, res, next)
        .catch(next);
  };
}

module.exports = {
  asyncWrapper,
};

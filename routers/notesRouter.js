const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {getUserNotes,
  addUserNote,
  getUserNoteByID,
  updateNote,
  toggleCheckNote,
  removeNote} = require('../controllers/notesController');
const {validateAddNote} = require('./middlewares/validationMiddleware');

router.use('/', authMiddleware);

router.get('/', asyncWrapper(getUserNotes));

router.post('/', validateAddNote, asyncWrapper(addUserNote));

router.get('/:id', asyncWrapper(getUserNoteByID));

router.put('/:id', validateAddNote, asyncWrapper(updateNote));

router.patch('/:id', asyncWrapper(toggleCheckNote));

router.delete('/:id', asyncWrapper(removeNote));

module.exports = router;

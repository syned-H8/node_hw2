const {Note} = require('../models/noteModel');
const {User} = require('../models/userModel');

module.exports.getUserNotes = async (req, res) => {
  const {username} = req.user;
  const user = await User.findOne({username});
  const {offset = 0, limit = 5} = req.query;
  const requestOptions = {
    skip: parseInt(offset),
    limit: parseInt(limit),
  };

  const notes = await Note.find(
      {userId: {$eq: user._id}, __v: 0},
      [],
      requestOptions,
  );

  res.status(200).json({
    notes,
  });
};

module.exports.addUserNote = async (req, res) => {
  const {text} = req.body;
  const {username} = req.user;
  const user = await User.findOne({username});

  const note = new Note({userId: user._id, text: text});

  await note.save();

  res.json({message: 'Success'});
};

module.exports.getUserNoteByID = async (req, res) => {
  const noteId = req.params.id;

  const note = await Note.findOne({_id: noteId});

  res.status(200).json({
    note,
  });
};

module.exports.updateNote = async (req, res) => {
  const _id = req.params.id;
  const {text} = req.body;

  const note = await Note.findOne({_id, userId: req.user._id});

  if (!note) {
    return res.status(400).json({message: 'File was not found!'});
  }

  note.text = text;
  note.save();

  res.status(200).json({
    message: 'Success',
  });
};

module.exports.toggleCheckNote = async (req, res) => {
  const _id = req.params.id;

  const note = await Note.findOne({_id, userId: req.user._id});

  if (!note) {
    return res.status(400).json({
      message: 'File was not found!',
    });
  }

  note.completed = note.completed === true ? false : true;
  note.save();

  res.status(200).json({
    message: 'Success',
  });
};

module.exports.removeNote = async (req, res) => {
  const _id = req.params.id;

  await Note.findOneAndRemove({_id, userId: req.user._id}, (error, result) => {
    if (error) {
      return res.status(500).json({
        message: 'Error',
      });
    }

    if (!result) {
      return res.status(400).json({
        message: 'Error',
      });
    }

    res.status(200).json({
      message: 'Success',
    });
  });
};

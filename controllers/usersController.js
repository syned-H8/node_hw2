const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

module.exports.getUserData = async (req, res) => {
  const {username} = req.user;
  const user = await User.findOne({username});

  if (!user) {
    return res
        .status(400)
        .json({message: `No user with username '${username}' found`});
  }

  res.status(200).json({
    user,
  });
};

module.exports.deleteUser = async (req, res) => {
  const {_id} = req.user;
  await User.findByIdAndDelete({_id});

  res.status(200).json({message: 'Success'});
};

module.exports.submitPassword = async (req, res, next) => {
  const {_id} = req.user;
  const {oldPassword} = req.body;
  const user = await User.findById({_id});

  bcrypt.compare(oldPassword, user.password, function(err, result) {
    if (err) {
      return res.status(500).json({message: 'Server error!'});
    }

    if (!result) {
      return res.status(400).json({message: 'Wrong password!'});
    }

    next();
  });
};

module.exports.changePassword = async (req, res) => {
  const {_id} = req.user;
  const {newPassword} = req.body;
  await User.findByIdAndUpdate(
      {_id},
      {$set: {password: await bcrypt.hash(newPassword, 10)}},
  );

  res.status(200).json({message: 'Success'});
};

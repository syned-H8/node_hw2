const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

module.exports.registration = async (req, res) => {
  const {username, password} = req.body;

  const user = new User({username, password: await bcrypt.hash(password, 10)});

  await user.save();

  res.json({message: 'User created successfuly!'});
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;
  const user = await User.findOne({username});

  if (!user) {
    return res.status(400)
        .json({message: `No user with username '${username}' found`});
  }

  bcrypt.compare(password, user.password, function(err, result) {
    if (err) {
      return res.status(500).json({message: 'Server error!'});
    };

    if (!result) {
      return res.status(400).json({message: 'Wrong password!'});
    }

    const token = jwt.sign({
      username: user.username,
      _id: user._id}, JWT_SECRET);

    res.json({message: 'Success', jwt_token: token});
  });
};

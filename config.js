module.exports = {
  JWT_SECRET: process.env.JWT_SECRET || 'secret',
  port: process.env.PORT || 8080,
};
